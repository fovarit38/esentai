<?php
include('./source/header.php')
?>
<div class="project1">

    <div class="newspage">
        <div class="container">
            <h4 class="page-title">NEWS</h4>
            <div>
                <div class="news-column">
                    <div class="news-column1">
                        <div class="column1-text">
                            <div class="column1-title">
                                <h4 class="text text-while text-s34">The Ritz-Carlton Opens 
                                    in Almaty
                                    By HolpitaliyNet</h4>
                            </div>
                            <div class="avtor text text-while text-s15">Авто статьи: Карлс Виктор</div>
                            <div class="paragraf-news text text-while text-s15">
                                Timeless luxury came to Almaty, Kazakhstan when The RitzCarlton opened
                                its first hotel in the country. The most anticipated hotel in Central
                                Asia was named by The RitzCarlton Hotel Company, L.L.C. and the international
                                real estate developer Capital Partners.
                                The new luxury hotel with 145 rooms offers unmatched elegance and is
                                the latest collaboration between Capital Partners and The RitzCarlton
                            </div>
                            <a class="readmorenews text text-while text-s17" href="news_single.php">READ MORE</a>
                        </div>
                    </div>
                </div>

                <div class="news-column">
                    <div class="news-column1">
                        <div class="column1-text">
                            <div class="column1-title">
                                <h4 class="text text-while text-s34">Esentai Tower & Mall </h4>
                            </div>
                            <div class="avtor text text-while text-s15">Авто статьи: Карлс Виктор</div>
                            <div class="paragraf-news text text-while text-s15">
                            Millennials are expected to be the greatest funders in history and the best-traveled generation.
                             Driven by these trends, fashion tourism has a new destination: Almaty, the former capital of
                              Kazakhstan and the largest metropolis in the region, is aimed at both nationals and visitors.
                               Esentai Tower and Mall embodies Almaty as a world-class shopping destination, home to the
                                RitzCarlton Hotel and a gallery of boutiques led by Louis Vuitton, Gucci, Prada and the
                                 Saks Fifth Avenue outpost.
                            </div>
                            <a class="readmorenews text text-while text-s17" href="news_single.phpl">READ MORE</a>
                        </div>
                    </div>
                </div>
                <div class="news-column">
                    <div class="news-column1">
                        <div class="column1-text">
                            <div class="column1-title">
                                <h4 class="text text-while text-s34">Level 8 innovational project By The Village  </h4>
                            </div>
                            <div class="avtor text text-while text-s15">Авто статьи: Карлс Виктор</div>
                            <div class="paragraf-news text text-while text-s15">
                            Capital Partners is a development company founded in 2001. For 17 years Capital Partners 
                            has realized large projects in Moscow, Istanbul and Bodrum. Capital Partners' major 
                            projects in Kazakhstan include Esentai Park, The Ritz-Carlton Hotel, Almaty, Esentai
                             Mall, Almaty Financial Center and Shymbulak ski resort.

                            </div>
                            <a class="readmorenews text text-while text-s17" href="news_single.php">READ MORE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include('./source/footer.php')
?>
