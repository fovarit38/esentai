<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
    />
    <link
            rel="stylesheet"
            href="https://unpkg.com/swiper@7/swiper-bundle.min.css"
    />
    <script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>

    <link rel="stylesheet" href="../css/style.css">
    <title>Esentai towers</title>


    <link rel="apple-touch-icon" sizes="57x57" href="/img/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">
    <link rel="manifest" href="/img/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

</head>
<body>
<div class="main">

    <header>
        <div class="deskTop">
            <div class="container">
                <div class="logo"><a href="./index.php"> <img src="../img/icons/logo.png"></a></div>
                <nav class="menu">
                    <ul class="menu">
                        <li><a href="/about.php">ABOUT US</a></li>
                        <li><a href="/infrastructurepage.php">INFRASTRUCTURE</a></li>
                        <li><a href="/projects.php">PROJECTS</a></li>
                        <li><a href="/news.php">PRESS</a></li>
                        <li><a href="/partners.php">PARTNERS</a></li>
                        <li><a href="/vacancies.php">VACANCIES</a></li>
                    </ul>
                </nav>
                <div class="btn-contact_us">
                    <a href="#">CONTACT US</a>
                </div>
                <div class="language">
                    <a class="lang" href="#">
                        <img src="/img/icons/map.png">
                        <span>Eng</span>
                        <img src="../img/icons/bottomarrow.png">
                    </a>
                </div>
            </div>
        </div>

        <div class="mobailTop">
            <div class="container mobailTop_body">
                <div class="menu_click">
                    <a href="javascript:void(0)" class="menu_icon menu_open">
                        <div></div>
                        <div></div>
                        <div></div>
                    </a>
                </div>
                <div class="logo logo-head logo-head-bk">
                    <img src="/img/icons/logo.png" alt="">
                </div>
                <div class="mini_icon"></div>
            </div>
        </div>
    </header>
