<?php
include('./source/header.php')
?>
<div class="projects1">
    <div class="news1">
        <div class="container">
            <h2>The Ritz-Carlton Opens in Almaty
                By HolpitaliyNet</h2>
            <p class="awtor">Авто статьи: Карлс Виктор</p>
            <img class="news1back" src="./img/news/news1/news1back.png">
            <p class="news1-text2">Timeless luxury came to Almaty, Kazakhstan when The RitzCarlton opened its first
                hotel in the country. The most anticipated hotel in Central Asia was named by The RitzCarlton Hotel
                Company, L.L.C. and the international real estate developer Capital Partners.</p>

            <p class="news1-text2">The new luxury hotel with 145 rooms offers unmatched elegance and is the latest
                collaboration between Capital Partners and The RitzCarlton after their successful partnership with The
                RitzCarlton, Moscow. Central Asia.With club and suite accommodations and more than 10,000 square feet of
                meeting facilities, it is part of Capital Partners' Esentai Park project, which also includes a
                sprawling mall and residential complexes.</p>

            <p>“We're excited to bring The RitzCarlton brand to Kazakhstan. Almaty is an important city for our
                customers and this hotel is a wonderful addition to the RitzCarlton portfolio of hotels around the
                world, ”said Herve Humler, President and COO of The RitzCarlton. . "Almaty is a dynamic city that
                attracts visitors from all over the world and we are happy to be here."</p>
            <p>Burak Oymen, co-founder of Capital Partners) said, “We are excited to be working with</p>
            <p class="news1-text2">RitzCarlton again to bring their prestigious hotel brand to Almaty.</p>

            <p class="news1-text2">“Capital Partners invests and develops projects all over the world, but our roots are
                firmly in Almaty and our continued investments here show our commitment and belief in the strength of
                the real estate market in this great city of world class infrastructure, in this particular case for
                discerning travelers, professionals and residents "</p>

            <p class="news1-text2"> Mr. Oymen added," We are also pleased to announce The Ritz Carlton Residences on the
                upper floors of the Tower. the best views of the city but you will also enjoy all the fantastic hotel
                amenities with the privacy and benefits of personal property. "</p>

            <p class="news1-text2">Almaty, formerly a major city in Kazakhstan, is the largest city in the republic and
                an important cultural and commercial center. Located between the Bolshaya and Malaya rivers, Almaty used
                to be one of the transit stations on the Silk Road and is now a vibrant commercial destination.Located
                in the foothills of TransIli Alatau in southeastern Kazakhstan, where the mountains form a breathtaking
                backdrop to the city's skyline, Almaty is fast becoming a prime shopping destination for visitors to the
                neighboring mall with the opening of luxury stores like Gucci and Fendi Esentai.</p>

            <p class="news1-text2">Almaty, which literally means "City of Apple Trees", also offers extraordinary
                natural beauty in the surrounding areas, such as the Charyn Canyon, Big Almaty Lake and the IleAlatau
                National Park, a spectacular and diverse natural area with 300 species of animals and birds. including
                snow leopard, Central Asian lynx and TeanShan brown bear.</p>


        </div>
    </div>
</div>

<?php
include('./source/footer.php')
?>

