<?php
include('./source/header.php')
?>
<div class="projects1">

    <div class="vacancy-page">
        <div class="container">
            <div class="misson-block">
                <div class="mission-block__column1">
                    <h2>MISSION:</h2>
                    <img src="./img/vacancy/icons/1.png">
                    <p>High standards of property management to realize<br/> your ambitions</p>
                </div>
                <div class="mission-block__column">
                     <img  src="./img/vacancy/img1.png">
                </div>
            </div>
        </div>
        <div class="values-block">
            <div class="container">
                <h2>VALUES:</h2>
                <div class="values-column-top">
                    <div class="values-column">
                        <img src="./img/vacancy/icons/2.png">
                        <p><b>Partnership:</b> We build trust and long-<br/>term relationships with our partners</p>
                    </div>
                    <div class="values-column">
                        <img src="./img/vacancy/icons/3.png">
                        <p><b>Leadership:</b> We strive for<br/> excellence on every level</p>
                    </div>
                    <div class="values-column">
                        <img src="./img/vacancy/icons/4.png">
                        <p><b>Taking care of employees:</b> We give<br/> opportunities for the constant<br/> development
                            and realization of our<br/> People's potential</p>
                    </div>
                
                
                    <div class="values-column">
                        <img src="./img/vacancy/icons/5.png">
                        <p><b>Unique business space:</b> We are constantly changing, following modern trends, creating
                            an inspiring environment and comfort for the development of a successful business</p>
                    </div>
                
                    <div class="values-column">
                    <img src="./img/vacancy/icons/6.png">
                    <p><b>Sustainability:</b> We recognize our responsibility to the next generation by caring for the
                        environment and using resources efficiently</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="vacancy-block">
            <div class="container">
                <div class="vacancy-column">
                    <div class="vacancy-part1"><img src="./img/vacancy/img2.png"></div>
                    <div class="vacancy-part2">
                        <h3>Responsibilities of<br/> <b>the accountant:</b></h3>
                        <p>Maintaining and reflecting primary documentation in the accounting records</p>
                        <p>Handle all bank current and currency accounts, treasury accounts</p>
                        <p>Reproduction of primary documentation in accordance with management accounting
                            requirements</p>
                        <p>Control and timely reporting of the account payable party transactions</p>
                        <p>Payroll and computation of taxes and obligatory payments to the budget related to wages</p>
                        <p>Settlements with suppliers and contractors</p>
                        <p>Receiving goods on import </p>
                        <p>Preparing and submitting tax and statistical reports</p>
                        <p>Completing management instructions</p>
                        <a href="#"><b>hr@esentaitower.com</b></a>

                    </div>

                </div>
                <div class="vacancy-column2">
                    <div class="vacancy-part1"><img src="./img/vacancy/img2.png"></div>
                    <div class="vacancy-part2">
                        <h3>Responsibilities of<br/> <b>the financial manager:</b></h3>
                        <p>Company budget - preparation and follow-up after approval</p>
                        <p>Budget forecast - quarterly update of the company budget </p>
                        <p>Risk management - identification, analysis and mitigation</p>
                        <p>Management reporting - preparation and analysis</p>
                        <p>Cash flow - managing the cash flow of the company</p>
                        <p>Financial reporting - supervise the project accountant</p>
                        <p>Financial analysis of business activities of the company </p>
                        <p>Reporting and terms of restrictions on loan and other agreements</p>

                        <a href="#"><b>hr@esentaitower.com</b></a>

                    </div>

                </div>
                <div class="vacancy-column">
                    <div class="vacancy-part1"><img src="./img/vacancy/img2.png"></div>
                    <div class="vacancy-part2">
                        <h3>Responsibilities of<br/> <b>the building engineer:</b></h3>
                        <p>Coordinating meetings with tenants and tenant contractors </p>
                        <p>Review and approve tenant projects for compliance with building regulations </p>
                        <p>Review projects, report back on findings</p>
                        <p>Deciding and responding to technical enquiries</p>
                        <p>Supervision of scope of works performed by contractors</p>
                        <p>Supervision of building works carried out by contractors</p>
                        <p>Control over compliance with the design solutions as per SNiP during construction and
                            erection works </p>
                        <p>Working with construction documents</p>
                        <p>
                            Negotiating works, collecting commercial offers, selecting contractors for drawing up a
                            quotation list between the contractors
                        </p>
                        <a href="#"><b>hr@esentaitower.com</b></a>

                    </div>

                </div>
            </div>
        </div>
    </div>
    
<?php
include('./source/footer.php')
?>
