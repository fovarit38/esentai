<?php
include('./source/header.php')
?>
<div class="projects1">

    <div class="page-projects">
        <div class="container">

            <div class="breadcrum">
                <div class="breadcrum-items">
                   <span class="text text-s15">
                   Home
                   </span>
                </div>
                <div class="breadcrum-items">
                   <span class="text text-s15">
                       News
                   </span>
                </div>
                
            </div>

            <div class="projects1-title">
                <h1>LEVEL 8</h1>
            </div>
            <p>Level 8 is a serviced office and a coworking area where customers can concentrate
                on their work and only pay for occupied space and rental time. The offices are
                fully equipped so that customers don't have to worry about their administration.<br/><br/>

                Level 8 has a total area of 1,600 square meters, customers have 24-hour access to
                the room, a security system, 14 furnished and equipped private offices, two executive
                rooms, three director's rooms, an office with eight assigned workstations, reception
                service, an Energy Pod Sleeping Area, five small and one large conference room, offices
                for confidential telephone calls, an equipped kitchen, a printing area, an office supplies
                department, a cloakroom, lockers and a large coworking area with a cafeteria.
            </p>
            <div class="contain2">
                <div class="contain2-column">
                    <div class="contain2-column-title">
                        <h5>PRIVATE </h5>
                        <p> OFFICE</p>
                    </div>
                    <div class="contain2-column-img"><img src="./img/page2/image 14.png"></div>
                </div>
                <div class="contain2-column2">

                    <div class="contain2-column-img"><img src="./img/page2/image 15.png"></div>
                    <div class="contain2-column-title">
                        <h5>DIRECTOR </h5>
                        <p> OFFICE</p>
                    </div>
                </div>
                <div class="contain2-column">
                    <div class="contain2-column-title">
                        <h5>CO-WORKING </h5>
                        <p> SPACE</p>
                    </div>
                    <div class="contain2-column-img"><img src="./img/page2/image 16.png"></div>
                </div>
                <div class="line"></div>
            </div>

        </div>

    </div>
</div>
<?php
include('./source/footer.php')
?>
