<?php
include('./source/header.php')
?>
<div class="wrap animate__animated " data-in="animate__zoomIn" data-out="animate__zoomOut">
    <div class="soc">
        <div class="youtube"><img src="../img/icons/youtube.png"></div>
        <div class="facebook"><img src="../img/icons/facebook.png"></div>
        <div class="mail">support@esentaitower.com</div>
        <div class="message"><img src="../img/icons/message.png"></div>
    </div>
    <div class="wrap-title">
        <div class="container">
            <div class="wrap_slid">
                <h2 class="text text-while text-s88">ESENTAI </h2>
                <h3 class="text text-while text-s133">TOWERS</h3>
            </div>
        </div>
    </div>

    <a href="#about" class="mouse" style="background-image: url('/img/mouse.png')">

    </a>
</div>

<div class="about" id="about">
    <div class="container ">
        <div>


            <div class="about-title text text-s34 animate__animated " data-in="animate__lightSpeedInLeft"
                 data-out="animate__lightSpeedOutLeft">
                ABOUT
            </div>
            <div class="about-top">
                <div class="about-top__column animate__animated " data-in="animate__lightSpeedInLeft"
                     data-out="animate__zoomOut">
                    <img src="../img/icons/about/1.png">
                    <h4 class="text text-s15">Capital Tower Development</h4>
                    <p class="text text-s15">Is the Kazakhstani leader in premium corporate real
                        estate with the expertise of more than 15 years on
                        the market. Our activities are geared to keep global
                        standards in the corporate culture.

                    </p>
                </div>
                <div class="about-top__column animate__animated " data-in="animate__lightSpeedInLeft"
                     data-out="animate__lightSpeedOutLeft">
                    <img src="../img/icons/about/2.png">
                    <h4 class="text text-s15">Mission</h4>
                    <p class="text text-s15">High standards of property management to realize your ambitions.

                    </p>
                </div>
                <div class="about-top__column animate__animated " data-in="animate__lightSpeedInLeft"
                     data-out="animate__lightSpeedOutLeft">
                    <img src="../img/icons/about/3.png">
                    <h4 class="text text-s15">VALUES</h4>
                    <p class="text text-s15">We strive for excellence on every level
                    </p>
                </div>
                <div class="about-top__column animate__animated " data-in="animate__lightSpeedInLeft"
                     data-out="animate__lightSpeedOutLeft">
                    <img src="../img/icons/about/4.png">
                    <h4 class="text text-s15">The buildings are equipped</h4>
                    <p class="text text-s15">With engineering heating systems operating on the principle
                        of a thermal curtain. Known as central glycol-based IT
                        cooling system.

                    </p>
                </div>
            </div>
        </div>
        <div class="about-bottom__title animate__animated " data-in="animate__lightSpeedInLeft"
             data-out="animate__lightSpeedOutLeft">
            <h2 class="text text-s99">ESENTAI </h2>
            <h3 class="text text-s42">TOWERS</h3>
        </div>
        <div class="about-bottom">
            <div class="about-bottom__column animate__animated " data-in="animate__lightSpeedInLeft"
                 data-out="animate__lightSpeedOutLeft">
                <div class="about-btm__title">
                    <img src="../img/icons/about/5.png">
                    <h5 class="text text-s34">MICROCLIMATE</h5>
                </div>
                <p class="text text-s15">Also, the Esentai Tower’s ventilation & air conditioning-Microclimate
                    control system-center is zoned and is controlled by a single control
                    panel BMS (Building Management System from Honeywell) the system gives
                    opportunity to each zone to be controlled by its regulator installed
                    in the tenant's premises.
                </p>

            </div>
            <div class="about-bottom__column animate__animated " data-in="animate__lightSpeedInLeft"
                 data-out="animate__lightSpeedOutLeft">
                <div class="about-btm__title">
                    <img src="../img/icons/about/6.png">
                    <h5 class="text text-s34">LEED certification </h5>
                </div>
                <p class="text text-s15">Esentai Tower received Leadership in Energy and Environmental
                    Design (LEED) silver green building certification in 2018.LEED
                    certification is developed by the non-profit U.S. Green Building
                    Council, it is a rating system for the design, construction,
                    operation, and maintenance of green buildings which is aimed to
                    help building owners and operators be environmentally responsible
                    and use resources efficiently.
                </p>

            </div>


        </div>
        <div class="about-img">
            <img src="../img/icons/about/about.png">
        </div>
    </div>
</div>


<div class="infrast">
    <div class="container">
        <div class="infr-title animate__animated " data-in="animate__lightSpeedInLeft"
             data-out="animate__lightSpeedOutLeft">
            <h3>INFRASTRUCTURE</h3>
        </div>
        <div class="slider animate__animated " data-in="animate__lightSpeedInRight"
             data-out="animate__lightSpeedOutRight">
            <div class="slider_src">
                <div class="slider_src_text">
                    <h3 class="text text-while text-bold  text-s25">Restaurants & Cafes zone </h3>
                    <p class="text text-while text-s15">
                        has an exciting offering flavours of the different cuisines. The venue is immersed in
                        contemporary, urban architecture with spectacular views of Esentai Square.
                    </p>
                </div>
            </div>
        </div>

    </div>

    <div class="container_pagi   ">
        <a class="button-slider button-slider-page1 active" href="javascript:void(false)"></a>
        <a class="button-slider button-slider-page2" href="javascript:void(false)"></a>
        <a class="button-slider button-slider-page3" href="javascript:void(false)"></a>
        <a class="button-slider button-slider-page4" href="javascript:void(false)"></a>
        <a class="button-slider button-slider-page5" href="javascript:void(false)"></a>
        <a class="button-slider button-slider-page6" href="javascript:void(false)"></a>
    </div>
</div>

<div class="projects">
    <div class="container projects_box">
        <div class="projects-1">
            <div class="projects-1__column">
                <div class="project-1-title animate__animated " data-in="animate__lightSpeedInLeft"
                     data-out="animate__lightSpeedOutLeft">
                    <h3 class="text text-s34">PROJECTS</h3>
                </div>
                <div class="project-1-p animate__animated " data-in="animate__lightSpeedInLeft"
                     data-out="animate__lightSpeedOutLeft">
                    <h4 class="text text-s25">LEVEL 8</h4>
                    <p class="text text-s15">is an alternative workspace with private offices and coworking located on
                        the eighth floor of
                        Esentai Tower. <br/><br/>

                        Built according to the most advanced standards, incorporating high tech solutions it offers
                        flexible, cost efficient and inspiring work environment for small and medium sized
                        companies.<br/><br/>

                        Coworking is a brilliant community for collaboration and building network, as it offers
                        opportunities that are not usually found in traditional offices. <br/><br/>

                        Moreover, we give the privilege of having an address at Esentai Tower, which has become a
                        synonym of trust and reputation.</p>
                </div>
            </div>
            <div class="projects-1__column">
                <div class="project-1-title animate__animated " data-in="animate__lightSpeedInLeft"
                     data-out="animate__lightSpeedOutLeft">
                    <h3 class="press-project text text-s34">PRESS</h3>
                </div>
                <div class="project-1-img animate__animated " data-in="animate__lightSpeedInLeft"
                     data-out="animate__lightSpeedOutLeft">
                    <img src="../img/projects/hospitality.png">
                    <img class="forte" src="../img/projects/forbes.png">
                    <img src="../img/projects/village.png">
                </div>
            </div>


        </div>
        <div class="projects-2 animate__animated " data-in="animate__lightSpeedInRight"
             data-out="animate__lightSpeedOutRight">
            <div class="slider_project_1">
                <div class="swiper">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <?php
                        for ($i = 0; $i < 3; $i++) {
                            ?>
                            <div class="swiper-slide">
                                <img src="../img/projects/project2.png">
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="swiper">
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper">
        <!-- Slides -->

        <div class="swiper-slide">
            <div class="slider-page">
                <div class="container">
                    <div class="slider-content  " data-in="animate__lightSpeedInLeft"
                         data-out="animate__lightSpeedOutLeft">
                        <div class="slider-content__title"><h3>According to the<br/> most advanced</h3></div>
                        <div class="slider-content__data"><p>23 september</p></div>
                        <div class="slider-content__paragraf"><p>Built according to the most advanced standards,
                                incorporating high tech solutions it offers
                                flexible, cost efficient and inspiring work
                                environment for small and medium sized companies.
                            </p>
                            <a href="#">READ MORE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="swiper-slide">
            <div class="slider-page">
                <div class="container">
                    <div class="slider-content  " data-in="animate__lightSpeedInLeft"
                         data-out="animate__lightSpeedOutLeft">
                        <div class="slider-content__title"><h3>asdasd to the<br/> most advanced</h3></div>
                        <div class="slider-content__data"><p>23 september</p></div>
                        <div class="slider-content__paragraf"><p>Built according to the most advanced standards,
                                s
                            </p>
                            <a href="#">READ MORE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- If we need pagination -->
    <div class="swiper-pagination"></div>

    <!-- If we need navigation buttons -->
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>

    <!-- If we need scrollbar -->
</div>


<div class="partners">
    <div class="container">
        <div class="partners-title animate__animated " data-in="animate__lightSpeedInLeft"
             data-out="animate__lightSpeedOutLeft">
            <h4 class="press-project text text-s34">PARTNERS</h4>
        </div>
        <div class="partner-img animate__animated " data-in="animate__lightSpeedInLeft"
             data-out="animate__lightSpeedOutLeft">
            <img src="./img/icons/partners/partners.png">
            <img src="./img/icons/partners/partners2.png">
            <img src="./img/icons/partners/partners3.png">
        </div>
    </div>
</div>
<?php
include('./source/footer.php')
?>
