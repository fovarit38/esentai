<?php
include('./source/header.php')
?>
<div class="projects1">
    <div class="infrastpage">
        <div class="container">
            <div class="infrast1page">
                <div class="infrastpage-abzac1">
                    <h3>A-Class Offices in Almaty downtown. </h3>


                    <P>Esentai Tower is located in the central business district of Almaty city, has a prestigious
                        address to make your business noticeable. It is not only beautifully designed but, also,
                        packed with in-demand amenities and technological features. Offices in Esentai Tower offer
                        office rental space in Class A buildings to suit your professional needs.</P></div>
                <div class="infrastpage-abzac2"><h4>The technological revolution is pushing the boundaries of the
                        property management sphere. </h4>
                    <p>Esentai Tower is not an exception. Carrier water chillers well-known leading supplier of
                        high-technology heating, air-conditioning, and refrigeration solutions are installed in the
                        building – 4 cooling systems and 8 air-conditioning systems that are aimed to cool down the east
                        side offices of the tower.</p>

                    <p> Furthermore, 15 elevators from the global leader in the elevator and escalator industry and 12
                        facade lifts from the AESA brand are located in the business center. It is important to point
                        out that our building has a backup power supply for life-supporting accommodation by
                        diesel-generating plants. Including all the requirements of water supply, Esentai Tower is
                        equipped with a plumbing system, water filtration system, and water softening technologies.</p>

                    <p>Moreover, by taking into account the main trends for monitoring and controlling daily engineering
                        task systems the Building Management System was placed in the organizational structure of
                        Esentai Tower. By taking responsibility for the safety of employees and partners the Honeywell
                        Excel Life Safety fire protection system provides the ability to monitor, control, and store
                        information in a network of fire controllers in a building. In addition, the system includes
                        fire detection, fire automation and alarm system, fire evacuation system.</p>


                    </p></div>
                <div class="infrastpage-abzac3"><h4>Technically advanced parking systems to keep up with the times and
                        promptly respond to future needs.
                    </h4>
                    <p>For convenience, Esentai Tower has a secure 398-space car park which is located on the basement
                        levels of the building: B1, B2, B3 and B4 floors. Also, well-known Austrian global leading
                        provider of management and ticketing solution “SKIDATA” is integrated for a broad spectrum of
                        technical support and seamless parking experience.</p></div>
            </div>
            <div class="infrast2page">
                <img class="infrastpageback" src="./img/inf/infrastpage.png">
            </div>
        </div>
    </div>
</div>
<?php
include('./source/footer.php')
?>


